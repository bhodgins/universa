package Universa;

use warnings;
use strict;

sub import {
    my ($class, @modules) = @_;
    my $package = caller;

    foreach my $module (@modules) {

	eval "package $package; use Universa::$module;";
	warn $@ if $@;
    }
}

1;
