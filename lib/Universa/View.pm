package Universa::View;
# Base role for all Universa views - particularly this role acts as a common interface
# between the core and the view.

use Moose::Role;

1;
