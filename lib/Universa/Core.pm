package Universa::Core;

{
    $Universa::Core::VERSION = '2013062601';
}

use Moose;

use Universa qw(Server);
use IO::Socket;


with 'MooseX::SimpleConfig';


has 'port'       => (
    isa          => 'Int',
    is           => 'ro',
    required     => 1,
    );

has 'addr'       => (
    isa          => 'Str',
    is           => 'ro',
    );

has 'name'       => (
    isa          => 'Str',
    is           => 'ro',
    required     => 1,
    );

has 'maxconn'    => (
    isa          => 'Int',
    is           => 'ro',
    );

has '_server'    => (
    isa          => 'Universa::Server',
    is           => 'rw',
);

sub BUILD {
    my $self = shift;

    my $server = Universa::Server->new(
	listener => IO::Socket::INET->new(
	    LocalAddr => $self->addr    || '0.0.0.0',
	    LocalPort => $self->port    || 9001,
	    Listen    => $self->maxconn || SOMAXCONN,
	    Proto     => 'tcp',
	    Type      => SOCK_STREAM,
	)
    );

    $self->_server($server);
    $self->_server->run_all; # Fire up the acceptor.
}

__PACKAGE__->meta->make_immutable;
