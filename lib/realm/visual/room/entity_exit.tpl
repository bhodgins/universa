[% IF cause == 'player_posess' %]
  [% player %] takes [% entity %].
[% ELSIF cause == 'player_toss' %]
  [% player %] throws [% entity %] [% direction %].
[% ELSIF cause == 'player_trash %]
  [% player %] destroys [% entity %].
[% ELSE %]
  [% entity %] fades out of existance.
[% END %]