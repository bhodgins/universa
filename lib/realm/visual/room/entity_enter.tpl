[% IF cause == 'player' %]
  [% player %] places [% entity %].
[% ELSIF cause == 'direction' %]
  [% entity %] flies in from the [% direction %].
[% ELSE %]
  [% entity %] fades into existance.
[% END %]